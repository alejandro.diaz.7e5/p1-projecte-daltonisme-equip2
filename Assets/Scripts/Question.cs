using UnityEditor;
using UnityEngine;

public class Question
{
    public string cardId;
    public string sprite;
    public string type; 
    // number cards ("NUMBER") and road cards ("ROAD") need different innput 
    public string question;
    public int[] solutions = new int[4];
    /*
     * solutions[0]: normal vision answer
     * solutions[1]: protanopia color blind answer
     * solutions[2]: deuteranopia color blind answer
     * solutions[3]: total color blindness answer
    */

    public Question(string cardId, string sprite, string type, string question, int[] solutions)
    {
        this.cardId = cardId;
        this.sprite = sprite;
        this.type = type;
        this.question = question;
        this.solutions = solutions;
    }

    public override string ToString()
    {
        return $"Id: {cardId}" +
            $"\nSprite: {sprite}" +
            $"\nType: {type}" +
            $"\nQuestion: {question}" +
            $"\nSolutions: [{solutions[0]}, {solutions[1]}, {solutions[2]}, {solutions[3]}]";
    }
}
