using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class QuestionManager2 : MonoBehaviour
{
    public int? result;
    public int activeQuestion = -1;
    public TextAsset questionJson;
    public List<Question> questions;
    public Question question;
    public int redColorblindCount = 0;
    public int greenColorblindCount = 0;
    public int totalColorblindCount = 0;
    public Image image;
    public Text TextElement;

    // Awake is called once instance is created
    private void Awake()
    {
        questionJson = Resources.Load<TextAsset>("JSONs/QuestionsData");
        questions = GetQuestions.JsonToQuestionObject(questionJson);
    }
    // gameObject.SetActive(true) to trigger

    public void nextQuestion()
    {
        if (activeQuestion <= -1)
        {
            Debug.Log("Questions Started");
            activeQuestion++;
        }
        else if (activeQuestion >= questions.Count - 1)
        {
            //Persist�ncia de dades per poder fer el result management.
            PlayerPrefs.SetInt("red color blind", redColorblindCount);
            PlayerPrefs.SetInt("green color blind", greenColorblindCount);
            PlayerPrefs.SetInt("total color blind", totalColorblindCount);

            Debug.Log("Questions Ended");
            SceneManager.LoadScene(4);
            gameObject.SetActive(false);
            return;
        }
        else if (ProcessAnswer())
        {
            activeQuestion++;
        }
        else
            Debug.Log("Invalid answer received");
        question = questions[activeQuestion];
        TextElement.text = (activeQuestion + 1) + "/" + questions.Count; //Mostrar n�mero de pregunta actual
        image.sprite = Resources.Load<Sprite>(question.sprite);
    }
    // Start is called before the first frame update
    void Start()
    {
        nextQuestion();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    private bool ProcessAnswer()
    {
        /*if (result == null)
        {
            Debug.Log("Empty input received");
            return false;
        }*/
        Debug.Log(result);   
        //Recollir resultat segions el input del usuari, -1 si el usuari no veu res
        bool ValidAnswer = false;

        if (result == question.solutions[0])
        {

            Debug.Log("User answer was correct");
            ValidAnswer = true;
        }
        if (result == question.solutions[1])
        {
            Debug.Log("User anwser indicates protanopy");
            redColorblindCount++;
            ValidAnswer = true;
        }
        if (result == question.solutions[2])
        {
            Debug.Log("User answer indicates deuteranopy");
            greenColorblindCount++;
            ValidAnswer = true;
        }
        if (result == question.solutions[3])
        {
            Debug.Log("User answer indicates total color blindness");
            totalColorblindCount++;
            ValidAnswer = true;
        }
        return ValidAnswer;
    }
}
