using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

public class SaveResults
{
    public void SaveIntoJson(int redcolorBlindCount,int greencolorBlindCount, int totalColorBlindCount)
    {
        string jsonFile = "./SavedTestData.json";

        List<FinalData> finalDataList = new List<FinalData>();

        List<FinalData> currentSavedData = null;
        if (File.Exists(jsonFile)) currentSavedData = JsonConvert.DeserializeObject<List<FinalData>>(File.ReadAllText(jsonFile));
        if (currentSavedData != null) finalDataList = currentSavedData;
        finalDataList.Add(new FinalData(redcolorBlindCount, greencolorBlindCount, totalColorBlindCount));

        using (StreamWriter sw = new StreamWriter(jsonFile))
        {
            sw.WriteLine("[");
            for (int i = 0; i < finalDataList.Count; i++)
            {
                sw.WriteLine(JsonConvert.SerializeObject(finalDataList[i]));
                if (i != finalDataList.Count - 1) sw.WriteLine(",");
            }
            sw.WriteLine("]");
        }
    }
    
}
