using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public void LoadHelpScreen()
    {
        SceneManager.LoadScene(1);
    }
    public void LoadMenuScreen()
    {
        SceneManager.LoadScene(0);
    }
    public void LoadTestScreen()
    {
        SceneManager.LoadScene(2);
    }
    public void LoadResultScreen()
    {
        SceneManager.LoadScene(4);
    }
    public void doExitGame()
    {
        Application.Quit();
    }
}
