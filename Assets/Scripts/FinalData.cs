using System;

public class FinalData
{
    public string Id;
    public int RedColorBlindCount;
    public int GreenColorBlindCount;
    public int TotalColorBlindCount;
    public DateTime CurrentDateTime;

    public FinalData(int redcolorBlindCount,int greencolorBlindCount, int totalColorBlindCount)
    {
        RedColorBlindCount = redcolorBlindCount;
        GreenColorBlindCount = greencolorBlindCount;
        TotalColorBlindCount = totalColorBlindCount;
        CurrentDateTime = DateTime.Now;
        Id = GenerateId();
    }

    private string GenerateId()
    {
        return $"{RedColorBlindCount}{GreenColorBlindCount}{TotalColorBlindCount}{CurrentDateTime.Day}{CurrentDateTime.Month}{CurrentDateTime.Year}";
    }

    public override string ToString()
    {
        return $"{Id}, {RedColorBlindCount},{GreenColorBlindCount}, {TotalColorBlindCount}, {CurrentDateTime}";
    }
}
