using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour
{
    private int[] _buttonsPerQuestion = new int[] { 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 1, 1, 1, 2, 2, 1};
    private List<GameObject> _buttonsGenerated = new List<GameObject>();
    public int currentQuestion = 0;
    [SerializeField] 
    private GameObject _buttonObject;
    [SerializeField]
    private Transform _parentTransform;

    public void Start()
    {
        GenerateButtons();
    }

    private void GenerateButtons()
    {
        float[] relativePositionX = new float[0];
        int[] buttonOutput = new int[0];

        if (currentQuestion < 24) 
        { 
            relativePositionX = GetRelativePositionsX();
            buttonOutput = GetAllButtonOutput(); 
        }

        for (int i = 0; i < _buttonsPerQuestion[currentQuestion]; i++)
        {
            int outputValue = FindSpecificButtonOutput(buttonOutput);
            GenerateOneButton(i + 1, Convert.ToString(outputValue), new Vector3(
                _parentTransform.position.x * relativePositionX[i],
                _parentTransform.position.y * 0.25f), outputValue);
        }

        if (currentQuestion != 0 && currentQuestion != 23)
        {
            GenerateOneButton(_buttonsPerQuestion[currentQuestion] + 1, "Other/Nothing", new Vector3(
                _parentTransform.position.x * relativePositionX[relativePositionX.Length - 1],
                _parentTransform.position.y * 0.25f), -1);
        }
    }

    private GameObject GenerateOneButton(int id, string text, Vector3 position, int buttonOutput) 
    {
        GameObject button = Instantiate(_buttonObject, _parentTransform);
        button.name = $"Button{id}";
        button.transform.position = position;

        button.GetComponent<Button>().onClick.AddListener(() => { UpdateQuestion(buttonOutput); }); 

        button.GetComponentInChildren<TextMeshProUGUI>().text = text;
        switch (currentQuestion) 
        {
            case 18:
            case 19:
            case 20:
                if (id == 1) button.GetComponentInChildren<TextMeshProUGUI>().text = "I see one line";
                break;
            case 17:
                if (id == 1) button.GetComponentInChildren<TextMeshProUGUI>().text = "Two lines";
                if (id == 2) button.GetComponentInChildren<TextMeshProUGUI>().text = "Line at top";
                if (id == 3) button.GetComponentInChildren<TextMeshProUGUI>().text = "Line at bottom";
                break;
            case 21:
            case 22:
                if (id == 1) button.GetComponentInChildren<TextMeshProUGUI>().text = "Continuous line";
                if (id == 2) button.GetComponentInChildren<TextMeshProUGUI>().text = "Discontinuous line";
                if (id != 3) button.GetComponentInChildren<TextMeshProUGUI>().fontSize = 18;
                break;

            case 0:
                button.transform.position = new Vector3(
                    _parentTransform.position.x,
                    _parentTransform.position.y * 0.25f);
                break;

            case 23:
                button.transform.position = new Vector3(
                    _parentTransform.position.x,
                    _parentTransform.position.y * 0.25f);
                button.GetComponentInChildren<TextMeshProUGUI>().text = "I see one line";
                break;
        }
        _buttonsGenerated.Add(button);
        return button;
    }

    private float[] GetRelativePositionsX()
    {
        float[] relativePositionX = new float[0];

        switch (_buttonsPerQuestion[currentQuestion] + 1)
        {
            case 2:
                relativePositionX = new float[] { 0.75f, 1.25f };
                break;
            case 3:
                relativePositionX = new float[] { 0.5f, 1, 1.5f };
                break;
            case 4:
                relativePositionX = new float[] { 0.25f, 0.75f, 1.25f, 1.75f };
                break;
        }

        return relativePositionX;
    }

    private int FindSpecificButtonOutput(int[] buttonOutput)
    {
        for (int i = 0; i < buttonOutput.Length; i++)
        {
            if (buttonOutput[i] != -1)
            {
                int intToReturn = buttonOutput[i];
                buttonOutput[i] = -1;
                return intToReturn;
            }
        }

        return -2;
    }

    private int[] GetAllButtonOutput()
    {
        QuestionManager2 questionManager = FindObjectOfType<QuestionManager2>();

        foreach (var questionData in questionManager.questions)
        {
            if (Convert.ToInt32(questionData.cardId) == currentQuestion + 1)
                return questionData.solutions.Distinct().ToArray();
        }

        return new int[0];
    }

    public void UpdateQuestion(int result) 
    {
        currentQuestion++;
        ResultSender rs = gameObject.AddComponent<ResultSender>(); 
        rs.result = result;
        rs.SendResults();
        DestroyButtons();
        if (currentQuestion < 24) GenerateButtons();
    }

    private void DestroyButtons() 
    {
        foreach (var button in _buttonsGenerated)
            Destroy(button);
        _buttonsGenerated = new List<GameObject>();
    }
}
