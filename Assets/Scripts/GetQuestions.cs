using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GetQuestions
{
    public static List<Question> JsonToQuestionObject(TextAsset questionList)
    {
;

        List<Question> questionsList = 
            JsonConvert.DeserializeObject<List<Question>>(questionList.text);

        foreach (var question in questionsList)
        {
            if (question.type == "NUMBER")
                question.question = "Introdueix el n�mero que pots veure o selecciona 'Res' si no veus cap n�mero:";
            else if (question.type == "PATH-1LINE")
                question.question = "Introdueix el cam� que pots veure o 'Res' si no veus cap cam�:";
            else if (question.type == "PATH-2LINE")
                question.question = "Introdueix el cam� o camins que pots veure o 'Res' si no veus cap cam�:";
            Debug.Log(question);           
        }
        return questionsList;
    }
}
