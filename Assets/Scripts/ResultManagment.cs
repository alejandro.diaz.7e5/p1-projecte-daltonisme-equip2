using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultManagment : MonoBehaviour
{
    public Text TextElement;
    public int RedColorblindCount;
    public int GreenColorblindCount;
    public int TotalColorblindCount;

    public void Start()
    {
        RedColorblindCount = PlayerPrefs.GetInt("red color blind");
        GreenColorblindCount = PlayerPrefs.GetInt("green color blind");
        TotalColorblindCount = PlayerPrefs.GetInt("total color blind");
        SaveResultsJson();
        ShowResultsScreen();
    }
    public void ShowResultsScreen()
    {
        if (RedColorblindCount <3 && TotalColorblindCount <11  && GreenColorblindCount<3)
        { TextElement.text = "Tens la vista perfecte."; }
        else if (TotalColorblindCount >= 11)
        { TextElement.text = "Ets totalment cec"; }
        else if (RedColorblindCount > 3 &&GreenColorblindCount<RedColorblindCount)
        {
            if (RedColorblindCount>3&&RedColorblindCount<=5)
            {
                TextElement.text = "Tens Deuteranopia: fluix";
            }
            else if (RedColorblindCount > 5 && RedColorblindCount <= 7)
            {
                TextElement.text = "Tens Deuteranopia: mitj�";
            }
            else if(RedColorblindCount>7)
            {
                TextElement.text = "Tens Deuteranopia: fort";
            }
        }
        else if (GreenColorblindCount > 3 &&RedColorblindCount<GreenColorblindCount)
        {
            if (GreenColorblindCount > 3 && GreenColorblindCount <= 5)
            {
                TextElement.text = "Tens Protanopia: fluix";
            }
            else if (GreenColorblindCount > 5 && GreenColorblindCount <= 7)
            {
                TextElement.text = "Tens Protanopia: mitj�";
            }
            else if (GreenColorblindCount > 7)
            {
                TextElement.text = "Tens Protanopia: fort";
            }
        }
        else TextElement.text = "Tens daltonisme";

    }
    /*public void ShowResult()
    {
        if (this.gameObject.GetComponent<QuestionManager>().colorblindCount > this.gameObject.GetComponent<QuestionManager>().activeQuestion / 2)
        {
            Debug.Log("You have color blind");
        }
        else Debug.Log("You don't have color blind");
    }*/
    public void SaveResultsJson()
    {
        SaveResults saveResults = new SaveResults();
        saveResults.SaveIntoJson(RedColorblindCount,GreenColorblindCount, TotalColorblindCount);
    }
}
