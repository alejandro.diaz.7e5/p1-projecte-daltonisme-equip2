using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultSender : MonoBehaviour
{
    public int result;
    QuestionManager2 qM;
    // Start is called before the first frame update
    void Start()
    { 
    }
    public void SendResults()
    {
        qM = GameObject.Find("questionManager").GetComponent<QuestionManager2>();
        qM.result = this.result;
        qM.nextQuestion();
    }
       
       

    // Update is called once per frame
    void Update()
    {
        
    }
}
